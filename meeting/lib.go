package meeting

import (
	"GoTempl/model"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"github.com/rs/xid"
	"strconv"
	"time"
)

type FormattedData struct {
	Id          string `db:"id"`
	Name        string `db:"name"`
	Description string `db:"description"`
	Date        string
	StartTime   string `db:"start_time"`
	EndTime     string `db:"end_time"`
	Finished    bool
}

func Index(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		listMeeting := make(model.ListMeeting, 0)

		err := db.Select(&listMeeting, "SELECT * FROM meeting")
		if err != nil {
			return err
		}

		listFormatted := make([]FormattedData, 0, len(listMeeting))
		for _, v := range listMeeting {
			listFormatted = append(listFormatted, FormattedData{
				Id:          v.Id,
				Description: v.Description,
				Date:        time.Unix(v.StartTime, 0).Format("2006-01-02"),
				StartTime:   time.Unix(v.StartTime, 0).Format("15:04"),
				EndTime:     time.Unix(v.EndTime, 0).Format("15:04"),
			})
		}

		return c.Render("meeting/index", fiber.Map{
			"Meetings": listFormatted,
		})
	}
}

func NewIndex() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		return c.Render("meeting/editor", fiber.Map{
			"Title":     "Add New Meeting",
			"urlAction": "/meeting/new",
		})
	}
}

func New(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {

		id := xid.New().String()
		roomId, err := strconv.Atoi(c.FormValue("roomId"))
		if err != nil {
			return fiber.NewError(400, "Invalid Room ID")
		}

		description := c.FormValue("meetDesc")
		date := c.FormValue("meetDate")
		if date == "" {
			return fiber.NewError(405, "Invalid Date")
		}

		startTime := c.FormValue("startTime")
		endTime := c.FormValue("endTime")
		if startTime == "" || endTime == "" {
			return fiber.NewError(405, "Invalid Time")
		}

		start, err := time.Parse("2006-01-02 15:04", date+" "+startTime)
		if err != nil {
			return err
		}
		end, err := time.Parse("2006-01-02 15:04", date+" "+endTime)
		if err != nil {
			return err
		}

		_, err = db.Exec("INSERT INTO meeting (id, room_id, description, start_time, end_time) VALUES (?, ?, ?, ?, ?)", id, roomId, description, start.Unix(), end.Unix())
		if err != nil {
			return err
		}

		return c.Redirect("/meeting")
	}
}

func EditIndex(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		id := c.Params("id")

		meeting := model.Meeting{}
		err := db.Get(&meeting, "SELECT * FROM meeting WHERE id = ?", id)
		if err != nil {
			return err
		}

		fmt.Println(time.Unix(meeting.StartTime, 0))
		fmt.Println(time.Unix(meeting.EndTime, 0))

		bind := fiber.Map{
			"Title":     "Edit Meeting",
			"urlAction": "/meeting/edit/" + id,
			"meetDesc":  meeting.Description,
			"meetDate":  time.Unix(meeting.StartTime, 0).Format("2006-01-02"),
			"startTime": time.Unix(meeting.StartTime, 0).Format("15:04"),
			"endTime":   time.Unix(meeting.EndTime, 0).Format("15:04"),
			"roomId":    meeting.RoomId,
		}

		fmt.Println(bind)
		return c.Render("meeting/editor", bind)
	}
}

func Edit(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		id := c.Params("id")

		roomId, err := strconv.Atoi(c.FormValue("roomId"))
		if err != nil {
			return fiber.NewError(400, "Invalid Room ID")
		}

		description := c.FormValue("meetDesc")
		date := c.FormValue("meetDate")
		if date == "" {
			return fiber.NewError(405, "Invalid Date")
		}
		startTime := c.FormValue("startTime")
		endTime := c.FormValue("endTime")
		if startTime == "" || endTime == "" {
			return fiber.NewError(405, "Invalid Time")
		}

		start, err := time.Parse("2006-01-02 15:05", date+" "+startTime)
		if err != nil {
			return err
		}
		end, err := time.Parse("2006-01-02 15:05", date+" "+endTime)
		if err != nil {
			return err
		}

		_, err = db.Exec("UPDATE meeting SET room_id = ?, description = ?, start_time = ?, end_time = ? WHERE id = ?", roomId, description, start.Unix(), end.Unix(), id)
		if err != nil {
			return err
		}

		return c.Redirect("/meeting")
	}
}

func Delete(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		id := c.Params("id")

		_, err := db.Exec("DELETE FROM meeting WHERE id = ?", id)
		if err != nil {
			return err
		}

		return c.Redirect("/meeting")
	}
}
