package room

import (
	"GoTempl/model"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"strconv"
)

func NewIndex() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		return c.Render("room/editor", fiber.Map{
			"Title":     "Add New Room",
			"urlAction": "/room/new",
		})
	}
}

func EditIndex(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		data := new(model.Room)
		err := db.Get(data, "SELECT * FROM ROOM WHERE id = ?", c.Params("id"))
		if err != nil {
			return err
		}

		return c.Render("room/editor", fiber.Map{
			"Title":     "Edit Room",
			"roomName":  data.Name,
			"roomDesc":  data.Description,
			"capacity":  data.Capacity,
			"urlAction": fmt.Sprint("/room/edit/", c.Params("id")),
		})
	}
}

func Delete(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		id := c.Params("id")
		_, err := db.Exec("DELETE FROM room WHERE id = ?", id)
		if err != nil {
			return err
		}
		return c.Redirect("/room")
	}
}

func Edit(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		id := c.Params("id")
		roomName := c.FormValue("roomName")
		roomDescription := c.FormValue("roomDesc")
		roomCapacity, err := strconv.Atoi(c.FormValue("capacity"))
		if err != nil || roomDescription == "" || roomName == "" || id == "" {
			return fiber.NewError(405, "Invalid Data")
		}

		_, err = db.Exec("UPDATE room SET name = ?, description = ?, capacity = ? WHERE id = ?", roomName, roomDescription, roomCapacity, id)

		return c.Redirect("/room")
	}
}

func New(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		roomName := c.FormValue("roomName")
		roomDescription := c.FormValue("roomDesc")
		roomCapacity, err := strconv.Atoi(c.FormValue("capacity"))
		if err != nil || roomDescription == "" || roomName == "" {
			return fiber.NewError(405, "Invalid Data")
		}

		_, err = db.Exec("INSERT INTO room (name, description, capacity) VALUES (?, ?, ?)", roomName, roomDescription, roomCapacity)
		if err != nil {
			return err
		}
		return c.Redirect("/room")
	}
}

func Index(db *sqlx.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		listRoom := make(model.ListRoom, 0)

		err := db.Select(&listRoom, "SELECT * FROM room")
		if err != nil {
			return err
		}
		return c.Render("room/index", fiber.Map{
			"Rooms": listRoom,
		})
	}
}
