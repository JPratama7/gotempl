package main

import (
	"GoTempl/meeting"
	"GoTempl/room"
	"database/sql"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/log"
	"github.com/gofiber/template/html/v2"
	"github.com/jmoiron/sqlx"
	_ "modernc.org/sqlite"
)

func main() {
	engine := html.New("./views", ".html")

	app := fiber.New(fiber.Config{
		ErrorHandler: ErrorHandler(),
		Views:        engine,
	})

	conn, err := sql.Open("sqlite", "./database.db")
	if err != nil {
		panic(err)
	}
	conn.SetMaxOpenConns(1)

	db := sqlx.NewDb(conn, "sqlite")
	if db.Ping() != nil {
		panic(err)
	}

	app.Get("/", func(c *fiber.Ctx) error {
		return c.Render("index", fiber.Map{
			"Title": "Hello, World!",
		})
	})

	app.Group("/meeting").
		Get("/", meeting.Index(db)).
		Get("/new", meeting.NewIndex()).
		Post("/new", meeting.New(db)).
		Get("/edit/:id", meeting.EditIndex(db)).
		Post("/edit/:id", meeting.Edit(db)).
		Get("/delete/:id", meeting.Delete(db))

	app.Group("/room").
		Get("/", room.Index(db)).
		Get("/new", room.NewIndex()).
		Post("/new", room.New(db)).
		Get("/edit/:id", room.EditIndex(db)).
		Post("/edit/:id", room.Edit(db)).
		Get("/delete/:id", room.Delete(db))

	log.Errorf("Listening : %v", app.Listen(":5000"))
}
