package main

import "github.com/gofiber/fiber/v2"

func ErrorHandler() func(ctx *fiber.Ctx, err error) error {
	return func(ctx *fiber.Ctx, err error) error {
		return ctx.Render("error", fiber.Map{
			"Error": err.Error(),
		})
	}
}
