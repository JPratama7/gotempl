package model

type Room struct {
	Id          int    `db:"id"`
	Name        string `db:"name"`
	Description string `db:"description"`
	Capacity    string `db:"capacity"`
	Open        bool   `db:"open"`
}

type Meeting struct {
	Id          string `db:"id"`
	RoomId      int    `db:"room_id"`
	Description string `db:"description"`
	StartTime   int64  `db:"start_time"`
	EndTime     int64  `db:"end_time"`
	Finished    bool   `db:"finished"`
}

type ListRoom []Room
type ListMeeting []Meeting
